import React from 'react';
import { View, StyleSheet,Text } from 'react-native';
import { Button,Card, Title, Paragraph,List,ActivityIndicator,Checkbox,Snackbar } from 'react-native-paper';
import Store from 'rn-json-store';
import { withNavigation } from 'react-navigation';
import Toast from 'react-native-simple-toast';

export default withNavigation(class LinksScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.user.name}`,
    headerStyle: {
      backgroundColor:"transparent"
    },
    headerTitleStyle:{
      color:"#fff",
      flex:3
    },
    headerTintColor:"#fff"
  });
  constructor(props){
    super();
    const { navigation } = props;
    this.state = {
      navigation,
      user:{xuid:null,name:null,notify:null},
      userIndex:null
    };
  }
  componentWillMount(){
    // this.state.navigation.getParam("user",{})
    Store.get("users").then((users)=>{
      var foundUser = null;
      users.forEach((ele,i)=>{
        if (ele.name === this.state.navigation.getParam("user",{}).name){
          foundUser = ele;
          foundUserIndex = i;
        }
      })
      console.log(foundUser);
      this.setState({user:foundUser,userIndex:foundUserIndex});
    })
  }
  save = async () =>{
    var clonedUsers = [...await Store.get("users")];
    var newUser = {...clonedUsers[this.state.userIndex],...this.state.user};
    clonedUsers[this.state.userIndex] = newUser;
    Store.set("users",clonedUsers);
    this.props.navigation.goBack()
    Toast.show("Saved")
  }
  delete = async () =>{
    var clonedUsers = [...await Store.get("users")];
    clonedUsers.splice(this.state.userIndex,1);
    console.log(clonedUsers);
    Store.set("users",clonedUsers);
    this.props.navigation.goBack()
    Toast.show("Deleted");
  }
  press = (element) =>{
    switch(element) {
      case "notify":
        this.setState({
          user:{
            ...this.state.user,
            notify:!this.state.user.notify
          }
        })
      break;
      case "save":
        this.save();
      break;
      case "delete":
        this.delete()
      break;
      case "cancel":
        this.props.navigation.goBack()
      break;
      default:
        console.log(`${element} is not a pressable.`)
      break;
    }
  }

  render() {
    var user = this.state.user;
    return (
      <View style={styles.mainView}>
        <Card>
          <Card.Content>
            <List.Section>
              <List.Item
                title="Notify"
                left={() => <Checkbox
                  status={user.notify ? 'checked' : 'unchecked'}
                  onPress={()=>this.press("notify")}
                  />}
                onPress={()=>this.press("notify")}
                />
            </List.Section>
          </Card.Content>
          <Card.Actions>
            <Button onPress={()=>this.press("delete")} color="red">Delete</Button>
            <Button onPress={()=>this.press("cancel")}>Cancel</Button>
            <Button onPress={()=>this.press("save")}>Save</Button>
          </Card.Actions>
        </Card>
      </View>
    );
  }
})

const styles = StyleSheet.create({
  mainView:{
    padding:10,
  }
});
