import React from 'react';
import { View, StyleSheet,Text } from 'react-native';
import { Button,Card, Title, Paragraph,List,ActivityIndicator } from 'react-native-paper';
import moment from "moment";
import PTRView from 'react-native-pull-to-refresh';

export default class LinksScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.user.name}`,
    headerStyle: {
      backgroundColor:"transparent"
    },
    headerTitleStyle:{
      color:"#fff",
      flex:3,
    },
    headerTintColor:"#fff"
  });
  constructor(props){
    super();
    const { navigation } = props;
    this.state = {
      navigation,
      loading:false,
      presence:null
    };
  }
  componentWillMount(){
    this.getContent(this.state.navigation.getParam("user",{}))
  }
  async getContent(user){
    this.setState({loading:true})
    let response = await fetch(
      `https://xboxapi.com/v2/${user.xuid}/presence`,{
        headers: {
          "X-Auth": "9dd6f0e70e5862b54966d59e5649ab95219e880b"
        },
    });
    let responseJson = await response.json();
    console.log(responseJson);
    var presence = this.parsePresence(responseJson)
    this.setState({presence})
    this.setState({loading:false})
  }
  parsePresence(presence){
    var res = {
      online:null,
      device:null,
      lastSeen:{
        time:{
          timestamp:null,
          fromNow:null
        },
        title:null,
      },
      activity:{
        title:null
      }
    };
    res.online = presence.state === "Online";
    if(!res.online){
      if (presence.lastSeen){
        res.lastSeen.title = presence.lastSeen.titleName ? presence.lastSeen.titleName : "unknown";
        res.lastSeen.time.timestamp = presence.lastSeen.timestamp ?  new Date(presence.lastSeen.timestamp) : "unknown";
        res.lastSeen.time.fromNow = presence.lastSeen.timestamp ? moment(presence.lastSeen.timestamp).fromNow() : "unknown time ago";
        res.device = presence.lastSeen.deviceType ? presence.lastSeen.deviceType : "unknown device";
      }else{
        res.lastSeen.title = "unknown";
        res.lastSeen.time.timestamp = "unknown";
        res.lastSeen.time.fromNow = "unknown time ago";
        res.device = "unknown device";
      }
    }else{
      let titles = presence.devices[0].titles;
      var title = titles.filter((title)=>{
        return title.placement === "Full";
      })[0]

      res.activity.title = title.name;
      res.device = presence.devices[0].type ? presence.devices[0].type : "unknown device"
    }

    return res;
  }
  render() {
    return (
      <PTRView onRefresh={()=>this.getContent(this.state.navigation.getParam("user",{}))}>
        <View style={styles.mainView}>
          <Card>
            {!this.state.loading && <Card.Content style={{paddingBottom:15}}>
              <Title>{this.state.presence.online ? "Online" : "Offline"}</Title>
              {!this.state.presence.online && <Paragraph>Last seen {this.state.presence.lastSeen.time.fromNow} in {this.state.presence.lastSeen.title}</Paragraph>}
              {this.state.presence.online && <Paragraph>Playing {this.state.presence.activity.title}</Paragraph>}
              <Paragraph>Device: {this.state.presence.device}</Paragraph>
            </Card.Content>}
            {this.state.loading && <Card.Content style={{paddingTop:15}}>
              <ActivityIndicator animating={true} size="large" />
            </Card.Content>}
          </Card>
        </View>
      </PTRView>
    );
  }
}

const styles = StyleSheet.create({
  mainView:{
    padding:10,
  }
});
