import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Button,Card, Title, Paragraph,List,IconButton } from 'react-native-paper';
import { withNavigation } from 'react-navigation';
import Store from 'rn-json-store';

export default withNavigation(class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerStyle: {
      backgroundColor:"transparent"
    },
    headerTitleStyle:{
      color:"#fff",
      flex:3
    },
  });

  constructor(){
    super();
    this.state = {
      users:null,
      editing:false
    };
  }
  componentDidMount(){
    this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.start();
      }
    );
    this.start();
  }
  start = async () =>{
    this.setState({users:await Store.get("users")});
  }
  toggleEdit(){
    this.setState({editing:!this.state.editing});
  }
  EditIcon = (params)=>{
    if (this.state.editing){
      return <List.Icon {...params.props} icon="edit" style={{padding:0,margin:0}} />
    }else{
      return null
    }
  }
  userClick = (user) =>{
    if (!this.state.editing){
      this.props.navigation.navigate("User",{user})
    }else{
      this.props.navigation.navigate("Edit",{user})
    }
  }
  press = (element,params={}) =>{
    switch(element) {
      case "user":
        let user = params;
        if (!this.state.editing){
          this.props.navigation.navigate("User",{user})
        }else{
          this.props.navigation.navigate("Edit",{user})
        }
      break;
      case "toggleEdit":
        this.setState({editing:!this.state.editing});
      break;
      case "addUser":
        this.props.navigation.navigate("AddUser");
      break;
      default:
        console.log(`${element} is not a pressable.`)
      break;
    }
  }

  render() {
    return (
      <ScrollView style={styles.mainView}>
        <Card style={styles.feedCard}>
          <IconButton
            icon={this.state.editing ? "close" : "edit"}
            color="#fff"
            size={20}
            onPress={()=>this.press("toggleEdit")}
            style={{position: "absolute",top:0,right:0,zIndex: 1000}}
          />
          <Card.Content>
            <List.Section>
             {this.state.users && this.state.users.map((user, index) => {
                return <List.Item
                  title={user.name}
                  key={user.xuid}
                  onPress={()=>this.press("user",user)}
                  left={(props) => <this.EditIcon props={props}/>}
                  />
             })}
             {this.state.editing && <List.Item
               title="Add User"
               onPress={()=>this.press("addUser")}
               left={(props) => <List.Icon {...props} icon="add" style={{padding:0,margin:0}} />}
               />}
           </List.Section>
          </Card.Content>
        </Card>
      </ScrollView>
    );
  }
})

const styles = StyleSheet.create({
  mainView:{
    padding:10
  },
  feedCard:{
    marginBottom:10
  }
});
