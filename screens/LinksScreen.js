import React from 'react';
import { View, StyleSheet } from 'react-native';
import { ExpoLinksView } from '@expo/samples';

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Links',
    headerStyle: {
      backgroundColor:"transparent"
    },
    headerTitleStyle:{
      color:"#fff",
      flex:3
    },
  };
  render() {
    return (
      <View style={styles.mainView}></View>
    );
  }
}

const styles = StyleSheet.create({
  mainView:{
    padding:10,
  }
});
