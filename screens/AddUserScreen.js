import React from 'react';
import { View, StyleSheet,Text } from 'react-native';
import { Button,Card, Title, Paragraph,List,ActivityIndicator,Checkbox,Snackbar,TextInput } from 'react-native-paper';
import Store from 'rn-json-store';
import { withNavigation } from 'react-navigation';
import Toast from 'react-native-simple-toast';

export default withNavigation(class LinksScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: `Add User`,
    headerStyle: {
      backgroundColor:"transparent"
    },
    headerTitleStyle:{
      color:"#fff",
      flex:3
    },
    headerTintColor:"#fff"
  });
  constructor(props){
    super();
    const { navigation } = props;
    this.state = {
      navigation,
      user:{xuid:null,name:null,notify:true},
      loading:false
    };
  }
  componentWillMount(){

  }
  async getXuid(gamertag){
    let response = await fetch(
      `https://xboxapi.com/v2/xuid/${gamertag}`,{
        headers: {
          "X-Auth": "9dd6f0e70e5862b54966d59e5649ab95219e880b"
        },
    });
    let responseJson = await response.json();
    console.log(responseJson);
    return responseJson;
  }
  save = async () =>{
    this.setState({loading:true})
    var clonedUsers = [...await Store.get("users")];
    var newUser = {...this.state.user};
    var xuid = await this.getXuid(newUser.name);
    newUser.xuid = xuid;
    console.log(newUser);
    clonedUsers.push(newUser);
    Store.set("users",clonedUsers);
    this.props.navigation.goBack()
    Toast.show("Saved")
    this.setState({loading:false})
  }
  press = (element,params) =>{
    switch(element) {
      case "notify":
        this.setState({
          user:{
            ...this.state.user,
            notify:!this.state.user.notify
          }
        })
      break;
      case "name":
        let name = params;
        this.setState({
          user:{
            ...this.state.user,
            name:name
          }
        })
      break;
      case "save":
        this.save();
      break;
      case "cancel":
        this.props.navigation.goBack();
      break;
      default:
        console.log(`${element} is not a pressable.`)
      break;
    }
  }

  render() {
    var user = this.state.user;
    return (
      <View style={styles.mainView}>
        <Card>
          {!this.state.loading && <Card.Content>
            <List.Section>
              <TextInput
                label='Name'
                value={user.name}
                style={{minWidth:'100%',color:"white"}}
                onChangeText={text => this.press("name",text)}
                />
              <List.Item
                title="Notify"
                left={() => <Checkbox
                  status={user.notify ? 'checked' : 'unchecked'}
                  onPress={()=>this.press("notify")}
                  />}
                onPress={()=>this.press("notify")}
                />
            </List.Section>
          </Card.Content>}
          {!this.state.loading &&
          <Card.Actions>
            <Button onPress={()=>this.press("cancel")}>Cancel</Button>
            <Button onPress={()=>this.press("save")}>Save</Button>
          </Card.Actions>}
          {this.state.loading && <Card.Content style={{paddingTop:15}}>
            <ActivityIndicator animating={true} size="large" />
          </Card.Content>}
        </Card>
      </View>
    );
  }
})

const styles = StyleSheet.create({
  mainView:{
    padding:10,
  }
});
