import React from 'react';
import { Platform,Text } from 'react-native';
import {  createBottomTabNavigator } from 'react-navigation';
import {createStackNavigator} from "react-navigation-stack"
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import tabBarIcon from '../components/tabBarIcon';

import HomeScreen from '../screens/HomeScreen';
import UserScreen from '../screens/UserScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import EditScreen from '../screens/EditScreen';
import AddUserScreen from '../screens/AddUserScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  User: UserScreen,
  Edit: EditScreen,
  AddUser:AddUserScreen,
},{
  headerMode: 'screen',
	headerLayoutPreset: 'center',
  cardStyle:{backgroundColor:'#333'}
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: tabBarIcon('home-outline'),
};

const LinksStack = createStackNavigator({
  Links: LinksScreen,
},{
  headerMode: 'screen',
  cardStyle:{backgroundColor:'#333'}
});

LinksStack.navigationOptions = {
  tabBarLabel: 'Links',
  tabBarIcon: tabBarIcon('link')
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
},{
  headerMode: 'screen',
  cardStyle:{backgroundColor:'#333'}
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: tabBarIcon('settings-outline'),
};

export default createMaterialBottomTabNavigator({
  LinksStack,
  HomeStack,
  SettingsStack,
},
{
    shifting: true,
    initialRouteName:"HomeStack"
});
