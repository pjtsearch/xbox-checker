import * as React from 'react';
import {View} from 'react-native';
import { BottomNavigation, Text,Appbar } from 'react-native-paper';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';

export default class MaterialNavigation extends React.Component {
  state = {
    index: 1,
    routes: [
      { key: 'links', title: 'Links', icon: 'link' },
      { key: 'home', title: 'Home', icon: 'home' },
      { key: 'settings', title: 'Settings', icon: 'settings' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    links: LinksScreen,
    home: HomeScreen,
    settings: SettingsScreen,
  });

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
        shifting={true}
      />
    );
  }
}
